---
title: "Development"
excerpt: "Help us build the Core product, Develop Ports, Lomiri and many more projects"
icon: '/img/community-center/developers.jpg'
layout: "article"
hideBreadcrumb: false
hideSidebar: false
showHeaderImage: false

subcategories:
  - name: "Porting Project"
    description: "Featuring a slick and easy-to-use interface based on the design of its predecessor, Canonical's Unity desktop environment, lomiri is the new name of Unity8 and all its related projects. Additionally, the interface that developers use to call on these projects will change. However, developers will only need to update to the new API when they'd like to package their apps for other distributions."
    page: "/development/porting"
  - name: "Core Development"
    description: "Whatever your goals salfmslkf sldfk lkj afjasd;l gvnasldgvnasldgvnasldgvnasld gjnasdlkfgas ;lfdjjslkjaldmalmd almdlamd.ams.,m, we're here to helyoe you trying to learn a new skill or get better at something you already know how to do? Are you looking for something to add to your CV for volunteering and community engagement? Maybe you just really love the mobile platform and want to see it thrive."
    page: "/development/core"
  - name: "Core Apps"
    description: "your measure of success. Are you trying to learn a new skill or get better at something you already know how to do? Are you looking for something to add to your CV for volunteering and community engagement? Maybe you just really love the mobile platform and want to see it thrive."
    page: "/development/apps"
  - name: "Halium Project"
    description: "Whatever y Are you trying to learn a new skill or get better at something you already know how to do? Are you looking for something to add to your CV for volunteering and community engagement? Maybe you just really love the mobile platform and want to see it thrive."
    page: "/development/halium"
  - name: "Clickable"
    description: "Whatever your goals are, we're here to help you be successful, no matter your measure of success. Are you trying to learn a new skill or get better at something you already know how to do? Are you looking for something to add to your"
    page: "/development/porting"

externalLinks:
  resources:
    - text: "Introduction to Clickable - Miguel Menéndez Carrion"
      category: "Blog"
      target: "#"
    - text: "Ubuntu Touch safety architecture"
      category: "Blog"
      target: "#"
  media:
    - text: "Florian Interview Episode #57"
      category: "Audiocast"
      target: "#"
    - text: "Dalton Interview Episode #56"
      category: "Audiocast"
      target: "#"
    - text: "Joan Ubucon Europ 19"
      category: "Youtube"
      target: "#"
  people:
    - name: "Dalton"
      target: "#"
    - name: "Florian"
      target: "#"
    - name: "Joan"
      target: "#"
  join:
    - text: "Forum OS Selection"
      icon: "yumi"
      target: "#"
    - text: "Forum Porting"
      icon: "yumi"
      target: "#"
    - text: "Porting"
      icon: "telegram"
      target: "#"
    - text: "Lomiri Dev"
      icon: "telegram"
      target: "#"
    - text: "Core Dev"
      icon: "telegram"
      target: "#"
    - text: "Halium project"
      icon: "telegram"
      target: "#"
    - text: "UBports core"
      icon: "irc"
      target: "#"
    - text: "UBports Core Apps"
      icon: "irc"
      target: "#"

onThisPage:
  - text: "Overview"
    target: "#content-overview"
  - text: "Core Development"
    target: "#content-core"
  - text: "App Development"
    target: "#content-app"
  - text: "Resources"
    target: "#content-resources"
  - text: "Media"
    target: "#content-media"
  - text: "People"
    target: "#content-people"
  - text: "Discussions"
    target: "#content-discussions"
  - text: "FAQ"
    target: "#faqs"

faq:
  - question: "Where do I report Ubuntu Touch bugs?"
    content: "Whatever your goals are, we're here to help you be successful, no matter your measure of success. Are you trying to learn a new skill or get better at something you already know how to do? Are you looking for something to add to your CV for volunteering and community engagement? Maybe you just really love the mobile platform and want to see it thrive."
---

# Welcome to Ubuntu Touch OS development in the nutshell

## Overview

To easy on you, we originized all UT layers so you also know how its works from the inside.

![Development layers](/img/development/layers.svg)

## Core Development

Our mission is to make Ubuntu Touch available on as many devices as possible

We create and maintain the behind-the-scenes components of Ubuntu Touch, taking care of all technical tasks and programming.

Our main responsibility is to improve the working features of Ubuntu Touch, fixing bugs, releasing new features and of course the ongoing work in Convergence.


## App Development

### What we do

We create Ubuntu Touch's core set of applications, and the tools and frameworks to allow people in the community to create apps for Ubuntu Touch.

We also hold the relationship with external app store services.

### What we are working on

Our goal is to make Ubuntu Touch available for as many devices as possible. Building a flawlessly working Operating System, Porting Ubuntu Touch for as many devices as possible
